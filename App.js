import React from 'react';
import Navigator from './app/navigators/navigator.js'

export default class App extends React.Component {
  constructor(props) {
    super(props)
  }

  render(){
    return(
      <Navigator />
    );
  
  }
}


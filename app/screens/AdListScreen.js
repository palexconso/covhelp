import fetch from "node-fetch";
import React from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';

export default class Adpage extends React.Component {
    constructor (props) {
        super(props);
        fetch('http://node02.waytostars.fr:43017/advert')
        .then(res => res.json())
        .then(json => {
            DATA = json
            this.forceUpdate()
        })
        .catch(err => { throw err });
        this.navigate = this.navigate.bind(this)
    }

    navigate (id) {
        this.props.navigation.navigate('AdView', { adId: 1 })
    }
    render () {
        return (
            <View style= {style.container}>
                <FlatList 
                 keyExtractor={(item)=> item.id.toString()} 
                 data={DATA} 
                 renderItem={({item})=>( 
                  <TouchableOpacity onPress={() => this.navigate(item.id)}>
                      <Text style={style.item}>{item.title}</Text>
                  </TouchableOpacity>)} />
            </View>
                    
        )
    }
}

let DATA = [
    { id: 0, title: 'Chargement...' }
];
const style= StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
        
    },
    item: {
        //width: '90%',
        //height: '15%',
        marginTop: 24,
        
        padding: 30,
        backgroundColor: '#24b2e0',
        color: 'white',
        fontSize : 20,
        //flexDirection: 'row',
        
    }
}) 

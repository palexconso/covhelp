import React from "react";
import { Text, SafeAreaView, Image, StyleSheet, ScrollView } from 'react-native';
import fetch from "node-fetch";

export default class AdViewScreen extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            title: "Example Title",
            description: "Loading...",
            image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==",
            contact: "",
            adId: -1
        };
        fetch(`http://node02.waytostars.fr:43017/advert/${this.props.route.params.adId}`)
        .then((response) => response.json())
        .then((json) => {
            this.props.navigation.setOptions({ headerTitle: json.title })
            this.setState({ adId: json.id, title: json.title, description: json.description, image: json.image, contact: json.contact });
        })
        .catch((err) => {
            this.props.navigation.setOptions({ headerTitle: "Erreur" })
            this.setState({ adId: -1, title: "Erreur", description: "Erreur de chargement de l'annonce..." });
        });
    }

    render () {
        let contactText = <Text style={styles.text}>Cette annonce n'a pas d'information de contact.</Text>
        if (this.state.contact.length !== 0) {
            contactText = <Text style={styles.text}>Contacter: {this.state.contact}.</Text>
        }
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <Image source={{ uri: this.state.image}} style={styles.image} />
                    <Text style={styles.text}>{this.state.description}</Text>
                    {contactText}
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    image: {
        width: "100%",
        height: 200,
        borderBottomWidth: 1, 
        borderColor: "#000000", 
        resizeMode: 'contain'
    },
    text: {
        margin: 10,
        fontSize: 17,
        textAlign: 'justify'
    }
})
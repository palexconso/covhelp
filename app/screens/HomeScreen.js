import React from "react";
import { SafeAreaView, ScrollView, Text,Image, StyleSheet, FlatList } from 'react-native';
import FlatButton from "../components/button";




export default class HomeScreen extends React.Component {
    
    constructor (props) {
        super(props);
        this.goToAdListPage = this.goToAdListPage.bind(this)
        this.goToCreateAd = this.goToCreateAd.bind(this)
    }
    goToAdListPage(){
        this.props.navigation.navigate("AdList")
    }
    goToCreateAd(){
        this.props.navigation.navigate("AdCreate")
    }
    


    render () {
        return (
            <SafeAreaView>
                <ScrollView style={{ height: "100%" }}>
                    <Image 
                    style ={{width: "100%", height:300, resizeMode: "contain"}}
                    source={require('../components/covid.png')}
                    />
                    <FlatButton text="Annonces" onPress={this.goToAdListPage}/>
                    <FlatButton text="Créer une annonce" onPress={this.goToCreateAd}/>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const style= StyleSheet.create({
    view: {
        backgroundColor: 'white'
    },
    title: {
        textAlign: 'center',
        fontSize: 25,
        color: "lightblue",
        marginVertical: 10,
        fontWeight: 'bold',
        marginBottom: 60,
    },
    soustitre: {
        color: 'lightblue',
        fontWeight:'bold',
        fontSize: 16,
        margin: 10
    }
}) 
